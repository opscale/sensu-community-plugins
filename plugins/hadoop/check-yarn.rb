#!/usr/bin/env ruby
#
# YARN check plugin
# ===
#
# Released under the same terms as Sensu (the MIT license); see LICENSE
# for details.

require 'rubygems' if RUBY_VERSION < '1.9.0'
require 'sensu-plugin/check/cli'
require 'json'
require 'net/http'
require 'crack'

class CheckYarn < Sensu::Plugin::Check::CLI

  option :host,
    :description => "Yarn host",
    :short => '-w',
    :long => '--host HOST',
    :default => 'localhost'

  option :port,
    :description => "YARN JMX port",
    :short => '-P',
    :long => '--port PORT',
    :default => '8088'

  option :threshold,
    :short => '-t threshold memory in MB ',
    :proc => proc {|a| a.to_i },
    :default => 5000
  
  def run
    res = yarn_healthy?
    if res
      ok 'Healthy'
    else
      critical 'Unhealthy'
    end
  end

  def yarn_healthy?
    host     = config[:host]
    port     = config[:port]

    begin
      url = "http://#{host}:#{port}/ws/v1/cluster/nodes"
      res = Net::HTTP.get_response(URI.parse(url)).body.gsub("\n", "")
      stats = JSON.parse(res)
      nodes = stats['nodes']['node']

      total_used_mem = 0
      nodes.each do |node|
        total_used_mem += node['usedMemoryMB']
      end

      if total_used_mem > config[:threshold].to_i
        return false
      else
        return true
      end
    end
  end 

end

