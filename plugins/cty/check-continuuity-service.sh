#!/bin/bash

SERVICE=$1

if [[ "X${SERVICE}" == "X" ]]; then
  echo "Usage: check-continuuity-service <service name>"
  exit 1
fi

echo ${SERVICE}

PID=`ps -A -o pid,cmd|grep ${SERVICE} | grep -v grep |head -n 1 | awk '{print $1}'`

if [[ "X${PID}" == "X" ]]; then
    echo "Service ${SERVICE} is not running"
    exit 1
else
    echo "Service ${SERVICE} running"
    exit 0
fi
